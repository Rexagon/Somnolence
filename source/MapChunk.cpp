#include "MapChunk.h"

#include <algorithm>
#include <vector>

#include <SFML/Graphics/RenderTarget.hpp>

#include "Log.h"

namespace detail 
{
	struct TileFindByIdPredicate
	{
		TileFindByIdPredicate(size_t id) : id(id) {}

		size_t id;

		bool operator()(const Tile& tile) {
			return tile.id == id;
		}
	};
}

MapChunk::MapChunk() :
	m_tileset(nullptr),
	m_currentTileId(0), m_vertexBuffer(sf::Triangles), m_wasChanged(false), 
	m_debugShape(sf::LineStrip)
{
}

MapChunk::~MapChunk()
{
}

rect MapChunk::getAABB() const
{
	return rect();
}

void MapChunk::setTileset(sf::Texture * tileset)
{
	m_tileset = tileset;
}

sf::Texture * MapChunk::getTileset() const
{
	return m_tileset;
}

size_t MapChunk::addTile(const vec2 & position, const vec2 & origin, const vec2 & size, const vec2& textureOffset, const vec2& textureSize)
{
	Log::write(m_tiles.size(), "before");
	m_tiles.insert({ ++m_currentTileId, position, origin, size, textureOffset, textureSize });
	Log::write(m_tiles.size(), "after");
	m_wasChanged = true;

	return m_currentTileId;
}

void MapChunk::removeTile(size_t id)
{
	auto it = std::find_if(m_tiles.begin(), m_tiles.end(), detail::TileFindByIdPredicate(id));
	if (it != m_tiles.end()) {
		m_tiles.erase(it);
		m_wasChanged = true;
	}
}

const sf::VertexBuffer & MapChunk::getDebugShape() const
{
	return m_debugShape;
}

void MapChunk::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	if (m_wasChanged) {
		std::vector<sf::Vertex> vertices(m_tiles.size() * 6);

		size_t index = 0;
		for (const auto& tile : m_tiles) {
			vec2 screenPosition = math::toScreenCoords(tile.position);
			vec2 topLeft = screenPosition - tile.origin;
			vec2 topRight = topLeft + vec2(tile.size.x, 0.0f);
			vec2 bottomLeft = topLeft + vec2(0.0f, tile.size.y);
			vec2 bottomRight = topLeft + tile.size;

			vertices[index + 0].position = vertices[index + 3].position = topLeft;
			vertices[index + 1].position = topRight;
			vertices[index + 5].position = bottomLeft;
			vertices[index + 2].position = vertices[index + 4].position = bottomRight;

			vertices[index + 0].texCoords = vertices[index + 3].texCoords = tile.textureOffset;
			vertices[index + 1].texCoords = tile.textureOffset + vec2(tile.textureSize.x, 0.0f);
			vertices[index + 5].texCoords = tile.textureOffset + vec2(0.0f, tile.textureSize.y);
			vertices[index + 2].texCoords = vertices[index + 4].texCoords = tile.textureOffset + tile.textureSize;

			if (index == 0 || topLeft.x < m_AABB.left) {
				m_AABB.width += m_AABB.left - topLeft.x;
				m_AABB.left = topLeft.x;
			}
			if (index == 0 || topLeft.y < m_AABB.top) {
				m_AABB.height += m_AABB.top - topLeft.y;
				m_AABB.top = topLeft.y;
			}
			if (index == 0 || bottomRight.x > m_AABB.left + m_AABB.width) {
				m_AABB.width = bottomRight.x - m_AABB.left;
			}

			if (index == 0 || bottomRight.y > m_AABB.top + m_AABB.height) {
				m_AABB.height = bottomRight.y - m_AABB.top;
			}

			Log::write(m_AABB);

			index += 6;
		}

		if (m_tiles.empty()) {
			m_AABB = rect();
		}

		m_vertexBuffer.create(vertices.size());
		m_vertexBuffer.update(vertices.data());


		// DEBUG SHAPE
		sf::Vertex debugShapeVertices[5];
		debugShapeVertices[0] = debugShapeVertices[4] = vec2(m_AABB.left, m_AABB.top);
		debugShapeVertices[1] = vec2(m_AABB.left + m_AABB.width, m_AABB.top);
		debugShapeVertices[2] = vec2(m_AABB.left + m_AABB.width, m_AABB.top + m_AABB.height);
		debugShapeVertices[3] = vec2(m_AABB.left, m_AABB.top + m_AABB.height);

		for (size_t i = 0; i < 5; ++i) {
			debugShapeVertices[i].color = sf::Color::Red;
		}

		m_debugShape.create(5);
		m_debugShape.update(debugShapeVertices);
		// DEBUG SHAPE

		m_wasChanged = false;
	}

	states.texture = m_tileset;
	target.draw(m_vertexBuffer, states);
}

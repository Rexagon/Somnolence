#pragma once

#include <map>

#include <SFML/Graphics/RectangleShape.hpp>

#include "GameObject.h"
#include "Animation.h"

class Character : public sf::Drawable, public GameObject
{
public:
	Character();
	~Character();

	void update(const float dt) override;

	void setMovementSpeed(float speed);
	float getMovementSpeed() const;
	
protected:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void updateDirection(Direction direction) override;

private:
	float m_animationTimer;
	Animation* m_walkAnimation;
	std::map<char, sf::Texture*> m_animationSpriteSheets;

	sf::RectangleShape m_sprite;

	float m_movementSpeed;
};
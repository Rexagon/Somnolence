#include "Game.h"
#include "Core.h"

void Game::onInit()
{
	vec2 tileSize = vec2(TILE_WIDTH, TILE_HEIGHT);
	for (int i = -3; i < 7; ++i) {
		for (int j = -3; j < 7; ++j) {
			m_mapChunk.addTile(vec2(i, j), tileSize * 0.5f, tileSize, vec2(0.0f, 96.0f), tileSize);
		}
	}

	ResourceManager::bind<TextureFactory>("texture_tileset", "textures/tileset.png");
	m_mapChunk.setTileset(ResourceManager::get<sf::Texture>("texture_tileset"));

	onResize(vec2(Core::getWindow().getSize()));
}

void Game::onClose()
{
}

void Game::onUpdate(const float dt)
{
	if (Input::getKeyDown(Key::Escape)) {
		SceneManager::deleteScene();
		return;
	}

	m_character.update(dt);
}

void Game::onDraw(const float dt)
{
	// render scene to texture
	m_frameBuffer.clear(sf::Color(32, 31, 30));
	sf::RenderStates states;

	// render begin
	m_frameBuffer.draw(m_mapChunk);
	m_frameBuffer.draw(m_character);

	m_frameBuffer.draw(m_mapChunk.getDebugShape());
	// render end

	m_frameBuffer.display();

	// render frame rectangle
	Core::getWindow().draw(m_frameRectangle);
	sf::Shader::bind(nullptr);
}

void Game::onResize(const vec2& windowSize)
{
	vec2 halfSize = windowSize * 0.5f;

	// properly resizing
	Core::getWindow().setView(sf::View(halfSize, windowSize));

	// framebuffer initialization
	m_frameBuffer.create(static_cast<unsigned int>(windowSize.x), static_cast<unsigned int>(windowSize.y));
	m_frameRectangle.setSize(windowSize);
	m_frameRectangle.setTexture(&m_frameBuffer.getTexture(), true);

	m_frameBuffer.setView(sf::View(vec2(), halfSize));
}
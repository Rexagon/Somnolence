#pragma once

#include <set>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/VertexBuffer.hpp>

#include "Math.h"

namespace detail 
{
	struct Tile 
	{
		size_t id;
		vec2 position;
		vec2 origin;
		vec2 size;
		
		vec2 textureOffset;
		vec2 textureSize;
	};

	struct TileSortPredicate
	{
		bool operator()(const Tile& a, const Tile& b) {
			float depthA = a.position.x + a.position.y;
			float depthB = b.position.x + b.position.y;

			return depthA < depthB;
		}
	};
}

class MapChunk : public sf::Drawable
{
public:
	MapChunk();
	~MapChunk();

	rect getAABB() const;

	void setTileset(sf::Texture* tileset);
	sf::Texture* getTileset() const;

	size_t addTile(const vec2& position, const vec2& origin, const vec2& size, const vec2& textureOffset, const vec2& textureSize);
	void removeTile(size_t id);

	const sf::VertexBuffer& getDebugShape() const;

protected:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

private:
	sf::Texture* m_tileset;

	std::multiset<detail::Tile, detail::TileSortPredicate> m_tiles;
	size_t m_currentTileId;

	mutable sf::VertexBuffer m_vertexBuffer;
	mutable bool m_wasChanged;
	mutable rect m_AABB;

	mutable sf::VertexBuffer m_debugShape;
};
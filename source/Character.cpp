#include "Character.h"

#include <SFML/Graphics/RenderTarget.hpp>

#include "AnimationFactory.h"
#include "ResourceManager.h"
#include "TextureFactory.h"
#include "Input.h"

Character::Character() :
	m_animationTimer(0.0f), m_walkAnimation(nullptr), m_movementSpeed(1.2f)
{
	ResourceManager::bind<AnimationFactory>("animation_walk", "textures/player_walk.json");
	m_walkAnimation = ResourceManager::get<Animation>("animation_walk");

	std::map<char, std::string> spriteSheetTextures = {
		{ Down,			"down"			},
		{ Left,			"left"			},
		{ Right,		"right"			},
		{ Up,			"up"			},
		{ Down_Left,	"down_left"		},
		{ Down_Right,	"down_right"	},
		{ Up_Left,		"up_left"		},
		{ Up_Right,		"up_right"		},
	};

	for (const auto& it : spriteSheetTextures) {
		std::string textureName = "texture_player_walk_" + it.second;
		ResourceManager::bind<TextureFactory>(textureName, "textures/player_walk_" + it.second + ".png");
		m_animationSpriteSheets[it.first] = ResourceManager::get<sf::Texture>(textureName);
	}

	m_sprite.setSize(vec2(64.0f, 128.0f));
	m_sprite.setOrigin(32.0f, 128.0f - 32.0f);

	m_sprite.setTexture(m_animationSpriteSheets[getDirection()]);
	m_sprite.setTextureRect(m_walkAnimation->getFrame(0));
}

Character::~Character()
{
}

void Character::update(const float dt)
{
	vec2 vector;

	char playerDirection = 0;
	if (Input::getKey(Key::W)) {
		playerDirection = Up;
		vector = vec2(0.0f, 1.0);
	}
	else if (Input::getKey(Key::S)) {
		playerDirection = Down;
		vector = vec2(0.0f, -1.0);
	}

	if (Input::getKey(Key::A)) {
		playerDirection |= Left;
		vector += vec2(-1.0f, 0.0f);
	}
	else if (Input::getKey(Key::D)) {
		playerDirection |= Right;
		vector += vec2(1.0f, 0.0f);
	}

	if (playerDirection) {
		vector = math::normalized(vector) * m_movementSpeed * dt;

		move(vector);
		setDirection(static_cast<Direction>(playerDirection));

		m_sprite.setTextureRect(m_walkAnimation->getFrame(static_cast<int>(m_animationTimer * 50.0f)));
		m_animationTimer = std::fmodf(m_animationTimer + dt, m_walkAnimation->getFrameCount() * 20.0f);
	}
}

void Character::setMovementSpeed(float speed)
{
	m_movementSpeed = speed;
}

float Character::getMovementSpeed() const
{
	return m_movementSpeed;
}

void Character::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform.translate(getScreenPosition());
	target.draw(m_sprite, states);
}

void Character::updateDirection(Direction direction)
{
	m_sprite.setTexture(m_animationSpriteSheets[direction]);

	GameObject::updateDirection(direction);
}

#pragma once

static const float MATH_PI = 3.14159265f;
static const float MATH_E = 2.71828183f;

static const float PHYSICS_G = 9.81f;

static const float TILE_WIDTH = 64.0f;
static const float TILE_WIDTH_HALF = 32.0f;
static const float TILE_HEIGHT = 32.0f;
static const float TILE_HEIGHT_HALF = 16.0f;
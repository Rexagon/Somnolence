#include "MainMenu.h"
#include "Core.h"

#include "Game.h"

void MainMenu::onInit()
{
	onReturn();
}

void MainMenu::onClose()
{
	onLeave();
}

void MainMenu::onUpdate(const float dt)
{
	if (Input::getKeyDown(Key::Return)) {
		SceneManager::addScene<Game>();
		return;
	}
}

void MainMenu::onDraw(const float dt)
{
	Core::getWindow().clear(sf::Color(30, 30, 30));
}

void MainMenu::onLeave()
{
	Log::write("loaded new scene");
}

void MainMenu::onReturn()
{
	Log::write("returned to last scene");
}

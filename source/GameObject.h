#pragma once

#include "Math.h"

class GameObject
{
public:
	enum Direction {
		Down	= 1 << 0,
		Left	= 1 << 1,
		Right	= 1 << 2,
		Up		= 1 << 3,

		Down_Left	= Down | Left,
		Down_Right	= Down | Right,
		Up_Left		= Up | Left,
		Up_Right	= Up | Right
	};

	GameObject();
	virtual ~GameObject();
	
	virtual void update(const float dt) = 0;
	
	void setDirection(Direction direction);
	Direction getDirection() const;

	// Moves object in isometric coords
	void move(float x, float y);

	// Moves object in isometric coords
	void move(const vec2& vector);

	// Set position of object in isometric coords
	void setPosition(float x, float y);

	// Set position of object in isometric coords
	void setPosition(const vec2& position);

	// Position of object in isometric coords
	vec2 getPosition() const;

	// Position of object in screen coords
	vec2 getScreenPosition() const;

protected:
	virtual void updateDirection(Direction direction);
	virtual void updatePosition(const vec2& position);

private:
	Direction m_direction;
	vec2 m_position;
};
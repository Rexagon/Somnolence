#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTexture.hpp>

#include "SceneManager.h"

#include "Character.h"
#include "MapChunk.h"

class Game : public Scene
{
public:
	void onInit() override;

	void onClose() override;

	void onUpdate(const float dt) override;

	void onDraw(const float dt) override;

	void onResize(const vec2& windowSize) override;

private:
	sf::RectangleShape m_frameRectangle;
	sf::RenderTexture m_frameBuffer;
	
	MapChunk m_mapChunk;

	Character m_character;
};
#include "GameObject.h"

GameObject::GameObject() :
	m_direction(Down), m_position(0.0f, 0.0f)
{
}

GameObject::~GameObject()
{
}

void GameObject::setDirection(Direction direction)
{
	if (m_direction != direction) {
		updateDirection(direction);
	}
}

GameObject::Direction GameObject::getDirection() const
{
	return m_direction;
}

void GameObject::move(float x, float y)
{
	setPosition(m_position + vec2(x, y));
}

void GameObject::move(const vec2 & vector)
{
	setPosition(m_position + vector);
}

void GameObject::setPosition(float x, float y)
{
	if (m_position.x != x || m_position.y != y) {
		updatePosition(vec2(x, y));
	}
}

void GameObject::setPosition(const vec2 & position)
{
	if (m_position != position) {
		updatePosition(position);
	}
}

vec2 GameObject::getPosition() const
{
	return m_position;
}

vec2 GameObject::getScreenPosition() const
{
	return math::toScreenCoords(m_position);
}

void GameObject::updateDirection(Direction direction)
{
	m_direction = direction;
}

void GameObject::updatePosition(const vec2 & position)
{
	m_position = position;
}
